import chess
import pytest


def get_score(board):
    """Returns the score of the board. Good for white is a highly positive score
    good for black is a highly negative score. Checkmate is inf for white and -inf for black"""
    can_find_future_checkmate, checkmate_board = can_find_checkmate(board)
    if can_find_future_checkmate:
        return get_checkmate_score(checkmate_board)

    score = 0.0
    piece_values = [-1000000, 1, 3, 3, 5, 9, 0]  # mapped to chess.PIECE_TYPES, first index never used
    piece_type: chess.PieceType
    for square in chess.SQUARES:
        piece = board.piece_at(square)
        if piece:
            piece_value = piece_values[piece.piece_type]
            if piece.color == chess.WHITE:
                score += piece_value
            else:
                score -= piece_value
    return score


def get_checkmate_score(board):
    """Assumes board is checkmate, return positive if white made the last move (black turn)
       and -inf if black made the last move (white turn)"""
    if board.turn == chess.BLACK:
        return float('inf')
    return -float('inf')


def can_find_checkmate(board, depth=2) -> tuple[bool, chess.Board]:
    """Returns None if no checkmate can be found in given depth
    if checkmate can be found return inf for white getting checkmate and -inf for black"""
    if depth == 0:
        return False, board
    if board.is_checkmate():
        return True, board
    for move in board.legal_moves:
        board.push(move)
        is_checkmate, checkmate_board = can_find_checkmate(board, depth - 1)
        if is_checkmate:
            return is_checkmate, checkmate_board
        board.pop()

    return False, board


@pytest.fixture(name="white_up_pawn")
def white_up_pawn():
    """FEN where white is up by one pawn"""
    fen = "rnbqkbnr/1ppp1ppp/p7/4N3/4P3/8/PPPP1PPP/RNBQKB1R b KQkq - 0 3"
    return chess.Board(fen)


@pytest.fixture(name="black_up_knight_down_pawn")
def black_up_night_down_pawn():
    """FEN where black has taken a knight but is down a pawn"""
    fen = "rnbqkbnr/1pp2ppp/p7/4p3/2B1P3/8/PPPP1PPP/RNBQK2R w KQkq - 0 5"
    return chess.Board(fen)


@pytest.fixture(name="scholars_mate")
def scholars_mate():
    """Scholars mate game"""
    fen = "r1bqkbnr/ppp2Qpp/2np4/4p3/2B1P3/8/PPPP1PPP/RNB1K1NR b KQkq - 0 4"
    return chess.Board(fen)


@pytest.fixture(name="pre_scholars_mate_m1")
def pre_scholars_mate_m1():
    """The move before checkmate m1 of scholars mate"""
    fen = "r1bqkbnr/ppp2ppp/2np4/4p3/2B1P3/5Q2/PPPP1PPP/RNB1K1NR w KQkq - 2 4"
    return chess.Board(fen)


@pytest.fixture(name="white_down_material_but_m4")
def white_down_material_but_m4():
    """FEN where white is down 10 points of material but has mate in four"""
    fen = "r1b4r/p1p1q1pp/3b4/1p2P3/k7/5Q2/P1PB1PPP/n3R1K1 w - - 0 1"
    return chess.Board(fen)


@pytest.fixture(name="black_must_defend_checkmate")
def black_must_defend_checkmate():
    """FEN where white has checkmate on next move, but black can defend"""
    fen = "rnbqkbnr/ppp2ppp/3p4/4p3/2B1P3/5Q2/PPPP1PPP/RNB1K1NR b KQkq - 1 3"
    return chess.Board(fen)


def test_score_init():
    """Tests that score of initial board is zero"""
    board = chess.Board()
    assert 0.0 == get_score(board)


def test_score_white_up_pawn(white_up_pawn):
    """Tests that score of a board where white is up a pawn is 1"""
    assert 1.0 == get_score(white_up_pawn)


def test_score_black_up_knight_down_pawn(black_up_knight_down_pawn):
    """Tests that score of a board where black is up a knight but down a pawn is -2"""
    assert -2.0 == get_score(black_up_knight_down_pawn), "Black should be winning by 2 if up a knight and down a pawn"


def test_score_checkmate(scholars_mate, pre_scholars_mate_m1):
    """Tests score is positive infinity when checkmate"""
    assert float('inf') == get_score(scholars_mate), "Scholars mate should be max score"


def test_score_future_checkmate(pre_scholars_mate_m1):
    """Tests that next move is checkmate returns inf score"""
    assert float('inf') == get_score(pre_scholars_mate_m1)


def test_score_mate_if_black_does_nothing(black_must_defend_checkmate):
    """Test score should not be inifite if black can defend checkmate"""
    assert 0.0 == get_score(black_must_defend_checkmate)


def test_score_down_material_but_checkmate():
    """Tests that score is positive infinity despite down material"""
